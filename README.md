# Announcements

Programs are now uploaded.  
Tutorials will be updated at a later date.

# Desiderata

## Python (3.4)

## pip (3)

## NumPy

## SciPy

# Description of Programs

## train_rbm.py

## boltzmann_machines.py

## train_classifier.py

## linear_classifiers.py

## dataset.py

# How to Use
