import numpy as np
import pickle


def sigmoid(a):
    return 1.0 / (1.0 + np.exp(-a))


class BinaryRBM:
    """
    References:
    Hinton, G.E. (2002). Training products of experts by minimizing contrastive divergence, Neural Computation 14.
    Tieleman, T. (2008). Training Restricted Boltzmann Machines using Approximations to the Likelihood Gradient, ICML 2008.
    Tieleman, T. & Hinton, G.E. (2009). Using Fast Weights to Improve Persistent Contrastive Divergence, ICML 2009.
    Hinton, G.E. (2010). A Practical Guide to Training Restricted Boltzmann Machines, Technical Report.
    Fischer, A. & Igel, C. (2012). An Introduction to Restricted Boltzmann Machines, CIARP 2012.
    """
    def __init__(self, n_visible, n_hidden, rng=None):
        """
        rng (numpy.RandomState): Random number generator
        W (2D numpy.ndarray): Weights between visible and hidden units
        vb (1D numpy.ndarray): Biases for visible units
        hb (1D numpy.ndarray): Biases for hidden units
        lr (float): Learning rate for the regular parameters
        lr_decay (float): Decay rate for the learning rate
        n_steps (int): Number of steps for alternating Gibbs sampling
        fast_W (2D numpy.ndarray): Fast weights between visible and hidden units
        fast_vb (1D numpy.ndarray): Fast biases for visible units
        fast_hb (1D numpy.ndarray): Fast biases for hidden units
        fast_lr (float): Learning rate for the fast parameters
        fast_decay (float): Decay rate for the fast parameters
        fast_lr_decay (float): Decay rate for the fast learning rate
        vchains (2D numpy.ndarray): Markov chains for visible units
        algorithm (str): Learning algorithm to be used (CD, PCD, FPCD)

        *1 "..., the decay applied to the fast parameters was also close to optimal:
           multiplying them by 49/50 after each parameter update, as opposed to our heuristic 19/20,
           gave an improvement of 0.2 percentage points in the misclassfication rate"
           quoted from (Tieleman & Hinton, 2009) Sec.7.1.2.
        """
        self.rng = np.random.RandomState() if rng is None else rng
        self.W = self.rng.rand(n_hidden, n_visible)
        self.vb = np.zeros(n_visible)
        self.hb = np.zeros(n_hidden)
        self.lr = 1.0e-2
        self.lr_decay = 0.0
        self.n_steps = 1
        self.fast_W = None
        self.fast_vb = None
        self.fast_hb = None
        self.fast_lr = self.lr / 3.0
        self.fast_decay = 0.98  # *1
        self.fast_lr_decay = 0.0
        self.vchains = None
        self.algorithm = 'PCD'


    def set_learning_rate(self, learning_rate, n_epochs):
        """
        *1 "..., the learning rate was linearly decayed to zero, over the duration of the learning."
           quoted from (Tieleman, 2008) Sec.4.5.
        *2 "..., starting at about a third of the initial 'regular' learning rate"
           quoted from (Tieleman & Hinton, 2009) Sec.7.1.2.
        *3 "..., ending at the initial regular learning rate." quoted from (Tieleman & Hinton, 2009) Sec.7.1.2.
        """
        self.lr = learning_rate
        self.lr_decay = self.lr / n_epochs  # *1
        self.fast_lr = self.lr / 3.0  # *2
        self.fast_lr_decay = (self.lr - self.fast_lr) / n_epochs  # *3


    def save_params(self, filename):
        params = {
            'W': self.W, 'hb': self.hb, 'vb': self.vb,
            'fast_W': self.fast_W, 'fast_hb': self.fast_hb, 'fast_vb': self.fast_vb,
            'n_steps': self.n_steps, 'algorithm': self.algorithm
        }
        pickle.dump(params, open(filename, 'wb'), pickle.HIGHEST_PROTOCOL)


    def load_params(self, filename):
        params = pickle.load(open(filename, 'rb'))
        self.W = params['W']
        self.hb = params['hb']
        self.vb = params['vb']
        self.fast_W = params['fast_W']
        self.fast_hb = params['fast_hb']
        self.fast_vb = params['fast_vb']
        self.n_steps = params['n_steps']
        self.algorithm = params['algorithm']


    def gibbs_sampling(self, x):
        """
        Pseudocode is available in (Fischer & Igel, 2012) Sec.5.1 Algorithm 1.
        *1 Hidden units are binary. For details, see (Hinton, 2010) Sec.3.1, 3.4.
        *2 Visible units are real-valued. For details, see (Hinton, 2010) Sec.3.2, 3.4.
        """
        v = x.copy()
        for t in range(self.n_steps):
            ph_v = self.forward_propagate(v)
            h = self.sample(ph_v)  # *1
            v = self.backward_propagate(h)  # *2
        return v


    def sample(self, p):
        """
        *1 Hidden units are binary. For details, see (Hinton, 2010) Sec.3.1, 3.4.
        """
        return (p > self.rng.uniform(size=p.shape)).astype(float)  # *1


    def train(self, X, Y=None):
        if self.algorithm == 'CD' or self.algorithm == 'cd':
            self.contrastive_divergence(X)
        elif self.algorithm == 'PCD' or self.algorithm == 'pcd':
            self.persistent_contrastive_divergence(X)
        elif self.algorithm == 'FPCD' or self.algorithm == 'fpcd':
            self.fast_persistent_contrastive_divergence(X)


    def update_params(self, v0, vk):
        """
        'ph_v0' stands for p(h|v_0); 'ph_vk' for p(h|v_k)
        where 'k' is the number of steps for alternating gibbs sampling.
        """
        ph_v0 = self.forward_propagate(v0)
        ph_vk = self.forward_propagate(vk)
        self.W += self.lr * (np.outer(ph_v0, v0) - np.outer(ph_vk, vk))
        self.vb += self.lr * (v0 - vk)
        self.hb += self.lr * (ph_v0 - ph_vk)


    def contrastive_divergence(self, X):
        """
        Pseudocode is available in (Fischer & Igel, 2012) Sec.5.1 Algorithm 1.
        """
        for x in X:
            v = self.gibbs_sampling(x)
            self.update_params(x, v)
        self.lr -= self.lr_decay


    def persistent_contrastive_divergence(self, X):
        """
        Pseudocode is available in (Fischer & Igel, 2012) Sec.5.1 Algorithm1 or Sec.5.3 Algorithm 2.
        *1 "In the canonical form, there exists one Markov chain per training example in a batch."
           quoted from (Fischer & Igel, 2012) Sec.5.2.
        *2 "..., the learning rate was linearly decayed to zero, over the duration of the learning."
           quoted from (Tieleman, 2008) Sec.4.5.
        """
        if self.vchains == None:
            self.vchains = X.copy()  # *1

        for i, x in enumerate(X):
            v = self.vchains[i].copy()
            v = self.gibbs_sampling(v)
            self.vchains[i] = v.copy()
            self.update_params(x, v)
        self.lr -= self.lr_decay  # *2


    def update_params_and_fast_params(self, v0, vk):
        """
        'ph_v0' stands for p(h|v_0); 'ph_vk' for p(h|v_k) where 'k' is the number of steps for alternating gibbs sampling.
        *1 (Tieleman & Hinton, 2009) Sec.4
        *2 (Tieleman & Hinton, 2009) Sec.6
        """
        ph_v0 = sigmoid(np.dot(self.W, v0) + self.hb)
        ph_vk = sigmoid(np.dot(self.W + self.fast_W, vk) + self.hb + self.fast_hb)  # *1
        delta_W = np.outer(ph_v0, v0) - np.outer(ph_vk, vk)
        delta_vb = v0 - vk
        delta_hb = ph_v0 - ph_vk
        self.W += self.lr * delta_W
        self.vb += self.lr * delta_vb
        self.hb += self.lr * delta_hb
        self.fast_W = self.fast_decay * self.fast_W + self.fast_lr * delta_W  # *2
        self.fast_vb = self.fast_decay * self.fast_vb + self.fast_lr * delta_vb  # *2
        self.fast_hb = self.fast_decay * self.fast_hb + self.fast_lr * delta_hb  # *2


    def fast_persistent_contrastive_divergence(self, X):
        """
        Pseudocode is available in (Tieleman & Hinton, 2009) Sec.6.
        *1 "In the canonical form, there exists one Markov chain per training example in a batch."
           quoted from (Fischer & Igel, 2012) Sec.5.2.
        *2 Initialise the fast parameters to all zeros. For details, see (Tieleman & Hinton, 2009) Sec.6.
        *3 (Tieleman & Hinton, 2009) Sec.6
        *4 "..., the linearly-to-zero schedule turned out to be suboptimal."
           quoted from (Tieleman & Hinton, 2009) Sec.7.3.
        *5 "..., one that worked best was a linear increasing schedule" quoted from (Tieleman & Hinton, 2009) Sec.7.1.2.
        """
        if self.vchains == None:
            self.vchains = X.copy()  # *1
        if self.fast_W == None:
            self.fast_W = np.zeros_like(self.W)  # *2
        if self.fast_vb == None:
            self.fast_vb = np.zeros_like(self.vb)  # *2
        if self.fast_hb == None:
            self.fast_hb = np.zeros_like(self.hb)  # *2

        for i, x in enumerate(X):
            v = self.vchains[i].copy()
            for k in range(self.n_steps):
                ph_v = sigmoid(np.dot(self.W + self.fast_W, v) + self.hb + self.fast_hb)  # *3
                h = self.sample(ph_v)
                v = sigmoid(np.dot((self.W + self.fast_W).T, h) + self.vb + self.fast_vb)  # *3
            self.vchains[i] = v.copy()
            v0 = x.copy()
            vk = v.copy()
            self.update_params_and_fast_params(v0, vk)
        self.lr -= self.lr_decay  # *4
        self.fast_lr += self.fast_lr_decay  # *5


    def forward_propagate(self, v):
        """
        *1 (Hinton, 2010) Sec.2, Eq.7
        """
        return sigmoid(np.dot(self.W, v) + self.hb)  # *1


    def backward_propagate(self, h):
        """
        *1 (Hinton, 2010) Sec.2, Eq.8
        """
        return sigmoid(np.dot(self.W.T, h) + self.vb)  # *1
