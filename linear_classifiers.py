import numpy as np
from scipy.special import ndtri


class MultiClassSCW1:
    """
    References:
    Wang, J., Zhai, P. & Hoi, S.C.H. (2012). Exact Soft Confidence-Weighted Learning, ICML 2012.
    Hoi, S.C.H., Wang, J. & Zhao., P. (2012). LIBOL: A Library for Online Learning Algorithms.
    """
    def __init__(self, n_in, K, C, eta):
        """
        C (float): Aggressiveness parameter
        mu (list of 1D numpy.dnarrays): Mean vectors for the Gaussian
        Sigma (2D numpy.ndarray): Covariance matrix for the Gaussian
        eta (float): Confidence hyperparameter
        phi (float): Return value of the inverse cumulative function of the Gaussian (ndtri)
        K (int): Number of classes
        """
        self.mu = [np.zeros(n_in) for k in range(K)]
        self.Sigma = np.identity(n_in)
        self.C = C
        self.phi = ndtri(eta)
        self.phi2 = self.phi ** 2
        self.phi4 = self.phi ** 4
        self.psi = 1.0 + 0.5 * self.phi2
        self.zeta = 1.0 + self.phi2
        self.K = K


    def predict(self, x):
        return np.asarray([np.dot(mu, x) for mu in self.mu])


    def classify(self, x):
        return np.argmax(self.predict(x))


    def train(self, X, Y):
        for x, y in zip(X, Y):
            self.update(x, y)


    def update(self, x, y):
        """
        Pseudocode is available in (Wang et al., 2012) Sec.4 Algorithm 1 and (Hoi et al, 2012) Sec3.2.3 Algorithm 20.
        x (1D numpy.dnarray): Data vector
        y (int): True class label
        r (int): Predicted class label
        m (float): Margin
        v (float): Confidence
        l (float): Loss
        alpha, beta, u_squared (float): Temporary variables

        *1 ZeroDivisionError occurs when 'v' equals zero.
        """
        mu_x = self.predict(x)
        r = np.argmax(mu_x)
        if r == y:
            return "No need to update the parameters"
        Sigma_x = np.dot(self.Sigma, x)
        m = mu_x[y] - mu_x[r]
        v = np.dot(x, Sigma_x)
        if 0.0 <= v < np.finfo(float).eps:  # *1
            v += np.finfo(float).eps  # *1
        l = max(0.0, self.phi * np.sqrt(v) - m)

        if l <= 0.0:
            return "No need to update the parameters"
        alpha = min(self.C,
                    max(0.0,
                        (-m * self.psi + np.sqrt(0.25 * (m ** 2) * self.phi4 + v * self.phi2 * self.zeta)) / (v * self.zeta)
                    )
        )
        if alpha <= 0.0:
            return "No need to update the parameters"
        u_squared = 0.5 * (-alpha * v * self.phi + np.sqrt(self.phi2 * (v ** 2) * (alpha ** 2) + 4.0 * v))
        beta = alpha * self.phi / (u_squared + v * alpha * self.phi)
        self.mu[y] += alpha * Sigma_x
        self.mu[r] -= alpha * Sigma_x
        self.Sigma -= beta * np.dot(self.Sigma, np.dot(np.outer(x, x), self.Sigma))
