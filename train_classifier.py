import sys
import os
import numpy as np
import boltzmann_machines as bm
import linear_classifiers as cls
import dataset as ds


def get_n(filename, target, end):
    i = filename.index(target)
    j = filename[i:].index(end)
    return filename[i:][:j][len(target):]


def estimate(seed):
    rng = np.random.RandomState(seed)
    n_train = 50000
    n_valid = 10000
    X_others, Y_others, X_test, Y_test = ds.normalized_mnist()
    rng = np.random.RandomState(seed)
    X_others, Y_others = ds.shuffle_dataset(X_others, Y_others, rng)
    X_train = np.asarray(X_others[:n_train])
    Y_train = np.asarray(Y_others[:n_train])
    X_valid = np.asarray(X_others[n_train: n_train + n_valid])
    Y_valid = np.asarray(Y_others[n_train: n_train + n_valid])
    n_in = X_train.shape[1]
    n_classes = Y_train.max() + 1

    C_candidates = [1.0e-1, 1.0e-2, 1.0e-3]
    eta_candidates = [0.55, 0.6, 0.65, 0.7, 0.75, 0.80, 0.85, 0.9, 0.95]

    print('# C eta precision')
    for C in C_candidates:
        for eta in eta_candidates:
            scw = cls.MultiClassSCW1(n_in=n_in, K=n_classes, C=C, eta=eta)
            scw.train(X_train, Y_train)
            valid_score = [scw.classify(x) == y for x, y in zip(X_valid, Y_valid)].count(True)
            precision = valid_score / Y_valid.shape[0]
            print(C, eta, precision)


def experiment():
    if len(sys.argv) < 5:
        if len(sys.argv) == 2:
            seed = int(sys.argv[1])
            estimate(seed)
        else:
            print('seed aggressiveness confidence filename')
        exit(0)
    seed = int(sys.argv[1])
    C = float(sys.argv[2])
    eta = float(sys.argv[3])
    filename = sys.argv[4]

    n_train = 50000
    n_valid = 10000
    X_others, Y_others, X_test, Y_test = ds.normalized_mnist()
    rng = np.random.RandomState(seed)
    X_others, Y_others = ds.shuffle_dataset(X_others, Y_others, rng)
    X_train = np.asarray(X_others[:n_train])
    Y_train = np.asarray(Y_others[:n_train])

    n_in = X_train.shape[1]
    n_classes = Y_train.max() + 1
    n_hidden = int(get_n(filename, target='hid', end='_'))
    rbm = bm.BinaryRBM(n_in, n_hidden)
    rbm.load_params(filename)
    scw = cls.MultiClassSCW1(n_in=n_hidden, K=n_classes, C=C, eta=eta)

    extracted_X_train = np.asarray([rbm.forward_propagate(x) for x in X_train])
    extracted_X_test = np.asarray([rbm.forward_propagate(x) for x in X_test])
    scw.train(extracted_X_train, Y_train)
    test_score = [scw.classify(x) == y for x, y in zip(extracted_X_test, Y_test)].count(True)
    precision = test_score / Y_test.shape[0]
    epoch = int(get_n(filename, target='epoch', end='.'))
    print(epoch, precision)


if __name__ == '__main__':
    experiment()
