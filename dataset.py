from sklearn.datasets import fetch_mldata
import numpy as np


def normalized_mnist(n_train: int=60000):
    dataset_name = 'MNIST original'
    dataset = fetch_mldata(dataset_name)
    X = dataset['data'].astype(float) / 255.0
    Y = dataset['target'].astype(int)
    X_train = X[:n_train]
    Y_train = Y[:n_train]
    X_test = X[n_train:]
    Y_test = Y[n_train:]
    return X_train, Y_train, X_test, Y_test


def shuffle_dataset(X, Y, rng):
    dataset = list(zip(X, Y))
    rng.shuffle(dataset)
    shuffled_X, shuffled_Y = zip(*dataset)
    shuffled_X = np.asarray(shuffled_X)
    shuffled_Y = np.asarray(shuffled_Y)
    return shuffled_X, shuffled_Y
