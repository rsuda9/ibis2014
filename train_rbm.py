import sys
import os
import numpy as np
import boltzmann_machines as bm
import dataset as ds


def experiment():
    if len(sys.argv) < 6:
        print('algorithm{CD, PCD, FPCD} n_hidden learning_rate n_epochs seed')
        exit(0)
    algorithm = sys.argv[1]
    n_hidden = int(sys.argv[2])
    learning_rate = float(sys.argv[3])
    n_epochs = int(sys.argv[4])
    seed = int(sys.argv[5])

    n_train = 60000
    X_train, Y_train, X_test, Y_test = ds.normalized_mnist(n_train=n_train)
    rng = np.random.RandomState(seed)
    X_train, Y_train = ds.shuffle_dataset(X_train, Y_train, rng)

    n_visible = X_train.shape[1]
    rbm = bm.BinaryRBM(n_visible, n_hidden, rng)
    rbm.set_learning_rate(learning_rate, n_epochs)
    rbm.algorithm = algorithm

    current = os.getcwd()
    directory = '{}_hid{}_lr{}_iters{}_seed{}'.format(
        algorithm, n_hidden, learning_rate, n_epochs, seed
    )
    if not os.path.exists(directory):
        os.makedirs(directory)
    os.chdir(directory)
    filenames = ['epoch{}.pkl'.format(i) for i in range(1, n_epochs + 1)]
    print('# Training the RBM...')
    for epoch, filename in enumerate(filenames):
        rbm.train(X_train)
        rbm.save_params(filename)
        print('# epoch {}'.format(epoch + 1))
    os.chdir(current)


if __name__ == '__main__':
    experiment()
