# お知らせ

プログラムをアップロードしました．  
説明が不足している箇所がいつくかあるので，後日追加・修正される可能性があります．

# 必要なもの
DebianおよびUbuntuにおけるインストール方法を示します．  
Windowsユーザの方はそれぞれの公式サイトからインストーラをダウンロードし，インストールを行って下さい．

## Python (3.4)
DebianおよびUbuntuにはPythonはデフォルトでインストールされていますが，次のコマンドでインストールを行うことが出来ます．  
``$ sudo apt-get install python3``

## pip (3)

pipを用いるとライブラリの導入が簡単になります．  
``$ sudo apt-get install python-pip3``

## NumPy
``$ sudo pip3 install numpy``

## SciPy
``$ sudo pip3 install scipy``

## scikit-learn
pipを用いてインストールしたPython3用のscikit-learnは実行時にエラーを吐くので，
下記のリポジトリからダウンロードとインストールを行って下さい．

``$ sudo pip3 install git+https://github.com/scikit-learn/scikit-learn.git``

## このリポジトリのプログラム

ダウンロードして任意の場所に解凍するか，``git clone`` して下さい．

# プログラム
プログラム初回実行時にscikit-learnのAPIを用いてMNISTデータセットをダウンロードします．  
インターネット接続を確認して下さい．

## train_rbm.py
活性化関数がロジスティック・シグモイド関数のRestricted Boltzmann Machine (RBM)を訓練するプログラムです．  
訓練時にパラメタを保存するので，特徴抽出器として再利用できるようになっています．

## boltzmann_machines.py
活性化関数がロジスティック・シグモイド関数のRBMが定義されているプログラムです．

## train_classifier.py
RBMを用いて特徴抽出を行い，得られた特徴量を用いて多クラスSoft Confidence Weighted-1 (SCW-1)分類器の訓練・テストを行うプログラムです．

## linear_classifiers.py
多クラスSCW-1分類器が定義されているプログラムです．

## dataset.py
上記プログラムを補助するプログラムです．データセットに関する操作が定義されています．

# 使い方

Restricted Boltzmann Machine (RBM)の訓練を行うには次のようにプログラムを実行します：

``python3 train_rbm.py FPCD 500 0.1 20 1234``

* 第一引数はプログラム ``train_rbm.py``です
* 第二引数は学習アルゴリズムです．CD, PCD, FPCDのいずれかを指定して下さい．cd, pcd, fpcdでも構いません
* 第三引数は隠れ素子数です．非負の整数を値として渡して下さい
* 第四引数は初期学習率です．非負の実数値を値として渡して下さい
* 第五引数はデータセットの適用回数（いわゆる繰り返し回数）です．非負の整数を値として渡して下さい
* 第六引数は乱数の種（シード）です．非負の整数を値として渡して下さい

実行後，上記の例だと ``FPCD_hid500_lr0.1_iters20_seed1234`` という名前のディレクトリが作成され，
その中に訓練を行ったRBMのパラメタが記述された ``epoch1.pkl``, ``epoch2.pkl``, ..., ``epoch20.pkl`` という名前のpickleフォーマットのファイルが作成されます．

SCW-1分類器の訓練を行うには次のようにプログラムを実行します：

``python3 train_classifier.py 1234 0.1 0.7 FPCD_hid500_lr0.01_iters20_seed1234/epoch1.pkl >> result.txt``

* 第一引数はプログラム ``train_classifier.py`` です
* 第二引数は乱数の種（seed）です．非負の整数を値として渡して下さい
* 第三引数は積極性（aggressiveness）です．大きくなるほど線形分離不可能なデータに対して大きくパラメタを変更するよう振る舞います．正の実数値を値として渡して下さい
* 第四引数は信頼度（confidence）のハイパーパラメタです．分類器は「与えられた事例が正しく分類される確率」をこの信頼度以上にするように学習を行います．[0.5, 1.0]の実数値を値として渡して下さい
* 第五引数は特徴抽出器として用いるRBMのパラメタが記述されたpickleフォーマットファイルのファイル名です

``train_classifier.py`` は標準出力に「学習回数 精度」を表示します．  
上記のように出力をファイルにリダイレクトすることを ``epoch1.pkl`` から ``epoch20.pkl`` まで行うことで，
Gnuplot等を用いて学習過程を表すグラフを作成することが出来るようになります．  
また第三引数以降を省略することで，確認集合を用いてハイパーパラメタの値の推定を行うことが出来ます．
